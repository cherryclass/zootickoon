# 1 & 2
CREATE DATABASE IF NOT EXISTS Zoo;
USE Zoo;
-- show databases;
# 3
DROP TABLE IF EXISTS Races;
CREATE TABLE IF NOT EXISTS Races (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(60) NOT NULL,
    type_food ENUM ('Carnivore','Herbivore','Omnivore') NOT NULL,
    aquatique BOOL NOT NULL,
    duree_vie TINYINT NOT NULL
);

DROP TABLE IF EXISTS Animaux;
CREATE TABLE IF NOT EXISTS Animaux (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    race INT UNSIGNED NOT NULL, -- cle étrangère de la table RACES
    date_nais DATE NULL,
    sexe ENUM ('M','F') NULL,
    pseudo VARCHAR(40) NOT NULL,
    commentaire TEXT NULL
);

DROP TABLE IF EXISTS Personnels;
CREATE TABLE IF NOT EXISTS Personnels (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(40) NOT NULL,
    prenom VARCHAR(40) NOT NULL,
    date_nais DATE NULL,
    sexe ENUM ('M','F') NOT NULL,
    fonction VARCHAR(30) NOT NULL,
    salaire DECIMAL(7,2) NOT NULL
);

DROP TABLE IF EXISTS Enclos;
CREATE TABLE IF NOT EXISTS Enclos (
	zone CHAR(3) PRIMARY KEY,
    nom VARCHAR(40) NOT NULL,
    capacite SMALLINT NULL,
    taille SMALLINT NULL,
    eau BOOL NULL,
    responsable INT UNSIGNED NULL -- cle étrangère de la table Personnels...
);

DROP TABLE IF EXISTS Loc_animaux;
CREATE TABLE IF NOT EXISTS Loc_animaux (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    animal INT UNSIGNED NOT NULL, -- cle étrangère de la table Animaux
    enclos CHAR(3) NULL, -- cle étrangère de la table Enclos
    date_arrivee DATE NOT NULL,
    date_sortie DATE NULL -- NULL indique la présence actuelle dans l'enclos donc
);

DROP TABLE IF EXISTS Soigneurs;
CREATE TABLE IF NOT EXISTS Soigneurs (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    pers INT UNSIGNED NOT NULL, -- cle étrangère de la table Personnels
    race INT UNSIGNED NOT NULL  -- cle étrangère de la table Races
);
